{ lib, config, pkgs, ... }:

{
  services = {
    avahi = {
      enable = true;
      nssmdns6 = true;
      nssmdns4 = true;
    };
    pipewire = {
      enable = true;
      alsa.enable = true;
      alsa.support32Bit = true;
      jack.enable = true;
      pulse.enable = true;
      wireplumber.enable= true;
    };

    tailscale = {
      enable = true;
    };

    greetd = {
      enable = true;
      package = true;
      settings = {      
        default_session = {
          command = "tuigreet --cmd 'dbus-run-session sway'";
        };
      };
    };

    flatpak = {
      enable = true;
    };


    libinput = {
      enable = true;
    };

    xserver = {
      xkb = {
        layout = "us";
      };
    };

    printing = {
      enable = true;
      webInterface = true;
      cups-pdf = {
        enable = true;
      };
      # drivers = [ (pkgs.writeTextDir "share/cups/model/Xerox_B210_Series.ppd" (builtins.readFile ./drivers/Xerox_B210_Series.ppd)) ];
    };

#    snapper = {
#      configs = {
#        home = {
#          SUBVOLUME = "/home";
#          ALLOW_USERS = [ "joshua" ];
#          TIMELINE_CREATE = true;
#          TIMELINE_CLEANUP = true;
#          TIMELINE_MIN_AGE=60;
#          TIMELINE_LIMIT_HOURLY=6;
#          TIMELINE_LIMIT_DAILY=7;
#          TIMELINE_LIMIT_WEEKLY=2;
#          TIMELINE_LIMIT_MONTHLY=1;
#          TIMELINE_LIMIT_YEARLY=0;
#        };
#      };
#    };
    gvfs.enable = true;
    tumbler.enable = true;
    openssh.enable = true;

  };
}
