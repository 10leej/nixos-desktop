{ lib, config, pkgs, ... }:

{
  users = {
    users = {
      joshua= {
        isNormalUser = true;
        extraGroups = [ "wheel" "video" "audio" "networkmanager" "libvirtd" ];
        initialPassword = "iamjosh";
        home = "/home/joshua";
        description = "Joshua AE Lee";
      };
    };
  };
}
