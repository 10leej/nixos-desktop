{ config, lib, pkgs, modulesPath, ... }:

{
  hardware = {
    graphics = {
      enable = true;
      enable32Bit = true; # Enables support for 32bit libs that steam uses
      extraPackages = with pkgs; [ 
        intel-media-driver
        intel-compute-runtime
        intel-ocl
        vaapiIntel
        libvdpau-va-gl
      ];
    };
    steam-hardware = {
      enable = true;
    };
  };
  imports =
    [ (modulesPath + "/installer/scan/not-detected.nix")
    ];

  boot ={
    kernelPackages = pkgs.linuxPackages_latest; 
    initrd.availableKernelModules = [ "xhci_pci" "ahci" "nvme" "usbhid" "usb_storage" "sd_mod" "sr_mod" ];
    initrd.kernelModules = [ ];
    extraModulePackages = with config.boot.kernelPackages;
      [ v4l2loopback.out ];
    kernelModules = [ "kvm-intel" "v4l2loopback" ];
    kernelParams = [
      "quiet"
    ];
    consoleLogLevel = 3;
    extraModprobeConfig = ''
      options v4l2loopback exclusive_caps=1 card_label="Virtual Camera"
    '';
  };


  fileSystems."/" =
    { device = "/dev/disk/by-uuid/24d43376-469b-44c3-aaa9-d4d89f5e3ba1";
      fsType = "ext4";
   };

  fileSystems."/boot" =
    { device = "/dev/disk/by-uuid/79D4-757D";
      fsType = "vfat";
      options = [ "fmask=0077" "dmask=0077" ];
    };

  swapDevices =
    [ { device = "/dev/disk/by-uuid/20146b82-f4b3-41ca-b51c-005480e53d81"; }];


  # Enables DHCP on each ethernet and wireless interface. In case of scripted networking
  # (the default) this is the recommended approach. When using systemd-networkd it's
  # still possible to use this option, but it's recommended to use it in conjunction
  # with explicit per-interface declarations with `networking.interfaces.<interface>.useDHCP`.
  networking.useDHCP = lib.mkDefault true;
  # networking.interfaces.enp8s0.useDHCP = lib.mkDefault true;
  
  nixpkgs.hostPlatform = lib.mkDefault "x86_64-linux";
  powerManagement.cpuFreqGovernor = lib.mkDefault "powersave";
  hardware.cpu.intel.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;
}
