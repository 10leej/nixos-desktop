{ lib, config, pkgs, ... }:

{
  programs = {
    sway = {
      enable = true;
      extraSessionCommands = " ";
      wrapperFeatures = {
        gtk = true; 
        base = true;
      };
    };

    vim = {
      enable = true;
      defaultEditor = true;
    };

    xwayland = {
      enable = true;
    };

    # Some programs need SUID wrappers, can be configured further or are
    # started in user sessions.
    mtr ={
      enable = true;
    };

    gamemode = {
      enable = true;
    };

    gnupg.agent = {
      enable = true;
      enableSSHSupport = true;
    };

    steam = {
      enable = true;
      remotePlay.openFirewall = true; # Open ports in the firewall for Steam Remote Play
      dedicatedServer.openFirewall = true; # Open ports in the firewall for Source Dedicated Server
      # gamescopeSession = {
      #  enable = true;
      #  env = { };
      #  args = [ ];
      # };
      extraCompatPackages = with pkgs; [ proton-ge-bin ];
      package = pkgs.steam.override {
        extraPkgs = pkgs:
        with pkgs; [
          xorg.libXcursor
          xorg.libXi
          xorg.libXinerama
          xorg.libXScrnSaver
          libpng
          libpulseaudio
          libvorbis
          stdenv.cc.cc.lib
          libkrb5
          keyutils
        ];
      };
    };
    thunar = {
      enable = true;
      plugins = with pkgs.xfce; [
        thunar-archive-plugin
        thunar-volman
      ];
    };
    chromium = {
      enable = true;
      homepageLocation = "https://10leej.com";
      defaultSearchProviderSuggestURL = "https://search.brave.com/search?q=";
      defaultSearchProviderSearchURL = "https://search.brave.com/search?q=";
      defaultSearchProviderEnabled = true;
      extraOpts = {
        "BrowserSignin" = 0;
        "SyncDisabled" = true;
        "PasswordManagerEnabled" = false;
        "SpellcheckEnabled" = true;
        "SpellcheckLanguage" = [
          "de"
          "en-US"
        ]; 
      };
      extensions = [
          "gcbommkclmclpchllfjekcdonpmejbdp" # https everywhere
          "cjpalhdlnbpafiamejdnhcphjbkeiagm" # ublock origin
      ];
    };
  };
}
