{ lib, config, pkgs, ... }:

{
  nix = {
    settings = {
      max-jobs = 1; # how many packages do you want to compile at a time? 1 seems reasonable enough
      cores = 4; # this sets the -j argument for make. 4 is reasonable enough for me in terms of reproducability
      auto-optimise-store = true;
      experimental-features = [ "nix-command" "flakes" ];
    };
    gc = { # garbage collection
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 7d";
    };
  };

}
