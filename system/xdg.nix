{ lib, config, pkgs, ... }:
{
  xdg = {
    portal ={ 
      enable = true;
      xdgOpenUsePortal = true;
      wlr = {
        enable = true;
        settings = { };
      };
    # extraPortals = [ "xdg-desktop-portal-gtk" ];
    };
    mime ={
      enable = true;
      defaultApplications = {
        "text/html" = "firefox.desktop";
        "x-scheme-handler/http" = "firefox.desktop";
        "x-scheme-handler/https" = "firefox.desktop";
        "x-scheme-handler/about" = "firefox.desktop";
        "x-scheme-handler/unknown" = "firefox.desktop";
      };
    };
  };
}
