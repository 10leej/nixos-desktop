{
  fileSystems."/mnt/games" = {
    device = "192.168.0.12:/srv/games";
    fsType = "nfs";
    options = [ "x-systemd.automount" "x-systemd.idle-timeout=30" "noauto" ];
  };
  fileSystems."/mnt/media" = {
    device = "192.168.0.12:/srv/media";
    fsType = "nfs";
    options = [ "x-systemd.automount" "x-systemd.idle-timeout=30" "noauto" ];
  };

}
