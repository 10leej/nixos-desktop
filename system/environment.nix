{ lib, config, pkgs, ... };

{
  environment = {
    variables = {
      "QT_STYLE_OVERRIDE"="qt5ct";
    };
  };
}
