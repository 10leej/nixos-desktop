{ pkgs ? import <nixpkgs> { } }:
pkgs.stdenv.mkDerivation rec {
  pname = "wonder-draft";
  version = "1.1.8.2-b";

  src = ./Wonderdraft-${version}-Linux64.zip;
  /*
  src = pkgs.fetchurl {
    url = "https://cdn.humble.com/humblebundle/tailwindgamesllc_ChunI/Wonderdraft-1.1.8.2-b-Linux64.zip?gamekey=AHd2bDSvSqqrMkpp&t=st=1735848931~exp=1735935331~hmac=71676e668dafe7e87a6ac842cebe5826bd406b975d1ec73f1ec204c9b8319188";
    hash = "sha256-m74Tn5dePnUJ4yV3GxpQhqqIcake4Jv0A1/0or7Vi1Y=";
  };
  */

  nativeBuildInputs = [ pkgs.makeWrapper pkgs.unzip ];
  dontConfigure = true;
  dontBuild = true;
  dontPatchElf = true;

  unpackPhase = ''
    unzip $src
    ls -l
    '';

  installPhase =
    let rpath = pkgs.lib.makeLibraryPath [
      pkgs.xorg.libX11
      pkgs.xorg.libXrandr
      pkgs.xorg.libXext
      pkgs.xorg.libXrender
      pkgs.xorg.libXi
      pkgs.xorg.libXinerama
      pkgs.xorg.libXcursor
      pkgs.libGL
      pkgs.ocamlPackages.alsa
      pkgs.libpulseaudio
    ] + ":${pkgs.stdenv.cc.cc.lib}/lib64";
  in ''
    runHook preInstall
    mkdir -p $out/share/Wonderdraft $out/bin
    cp -a * $out/share/Wonderdraft
    install -Dt $out/share/applications Wonderdraft.desktop
    substituteInPlace $out/share/applications/Wonderdraft.desktop \
      --replace 'Exec=/opt/Wonderdraft/Wonderdraft.x86_64' 'Exec=${pname}'
    install -Dt $out/share/icons Wonderdraft.png
    interp="$(cat $NIX_CC/nix-support/dynamic-linker)"
    patchelf --set-interpreter $interp $out/share/Wonderdraft/Wonderdraft.x86_64
    patchelf --set-rpath ${rpath} $out/share/Wonderdraft/Wonderdraft.x86_64
    for f in `find $out/share/Wonderdraft -name *.so`; do
      patchelf --set-rpath ${rpath} $f
    done
    chmod +x $out/share/Wonderdraft/Wonderdraft.x86_64
    makeWrapper $out/share/Wonderdraft/Wonderdraft.x86_64 $out/bin/Wonderdraft
    runHook postInstall
  '';
}
