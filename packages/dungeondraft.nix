{ pkgs ? import <nixpkgs> { } }:
pkgs.stdenv.mkDerivation rec {
  pname = "dungeondraft";
  version = "1.1.0.6";

  src = ./Dungeondraft/Dungeondraft-${version}-Linux64.zip;
  # src = pkgs.fetchurl {
  #  url = https://cdn.humble.com/humblebundle/tailwindgamesllc_ChunI/Dungeondraft-1.1.0.6-Linux64.zip?gamekey=CBHVfA7r8CPA6YVZ&t=st=1736029279~exp=1736115679~hmac=9e4682f1e06a293b5efc9eec6aaf57ed62cc9b972ebde01e235cf5b2ba65e42c; 
  #  hash = "sha256-AQK9FP4/xtQRn1flVpxtSEoLBx93FD+WfvFD+xDwUBg=";
  # };

  nativeBuildInputs = [ pkgs.makeWrapper pkgs.unzip ];
  dontConfigure = true;
  dontBuild = true;
  dontPatchElf = true;

  unpackPhase = ''
    unzip $src
    ls -l
    '';

  installPhase =
    let rpath = pkgs.lib.makeLibraryPath [
      pkgs.xorg.libX11
      pkgs.xorg.libXrandr
      pkgs.xorg.libXext
      pkgs.xorg.libXrender
      pkgs.xorg.libXi
      pkgs.xorg.libXinerama
      pkgs.xorg.libXcursor
      pkgs.libGL
    ] + ":${pkgs.stdenv.cc.cc.lib}/lib64";
  in ''
    runHook preInstall

    mkdir -p $out/share/Dungeondraft $out/bin

    cp -a * $out/share/Dungeondraft

    install -Dt $out/share/applications Dungeondraft.desktop

    substituteInPlace $out/share/applications/Dungeondraft.desktop \
      --replace-warn 'Exec=/opt/Dungeondraft/Dungeondraft.x86_64' 'Exec=Dungeondraft'
  
    sed '6d,7d' $out/share/applications/Dungeondraft.desktop
    interp="$(cat $NIX_CC/nix-support/dynamic-linker)"

    patchelf --set-interpreter $interp $out/share/Dungeondraft/Dungeondraft.x86_64

    patchelf --set-rpath ${rpath} $out/share/Dungeondraft/Dungeondraft.x86_64

    for f in `find $out/share/Dungeondraft -name *.so`; do
      patchelf --set-rpath ${rpath} $f
    done

    chmod +x $out/share/Dungeondraft/Dungeondraft.x86_64

    makeWrapper $out/share/Dungeondraft/Dungeondraft.x86_64 $out/bin/Dungeondraft

    runHook postInstall
  '';
}
