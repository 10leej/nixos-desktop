# driver is here  https://download.support.xerox.com/pub/drivers/B210/drivers/linux/ar/Xerox_B210_Linux_PrintDriver_Utilities.tar.gz

{ pkgs ? import <nixpkgs> { } }:
pkgs.stdenv.mkDerivation rec {
  pname = "myprinter-${version}";
  version = "1.0";

  src = pkgs.fetchurl {
    url = "https://download.support.xerox.com/pub/drivers/B210/drivers/linux/ar/Xerox_B210_Linux_PrintDriver_Utilities.tar.gz";
    hash = "sha256-ulEEZwab8rC7E7DO5vJVFtpJCw0tWUCEYnPf4xqT6zo=";
  };
  
  nativeBuildInputs = [ pkgs.gnutar pkgs.curl ];
  dontConfigure = true;
  dontBuild = true;
  dontPatch = true;

  unpackPhase = ''
    tar xvf $src
    ls -l
  '';

  installPhase = 
  ''
    mkdir -p $out/share/cups/model/
    cp uld/noarch/share/ppd/Xerox_B210_Series.ppd $out/share/cups/model/
  '';
}
