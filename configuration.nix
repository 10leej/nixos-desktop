# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running `nixos-help`).

{ lib, config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./system/hardware.nix
      ./system/programs.nix
      ./system/nfs-mounts.nix
      ./system/services.nix
      ./system/users.nix
      ./system/xdg.nix
    ];

  nixpkgs.config = {
    allowUnfree = true;
    allowUnfreePredicate = pkg: builtins.elem (lib.getName pkg) [
      "steam"
      "steam-original"
      "steam-run"
    ];
  };

  # Use the systemd-boot EFI boot loader.
  boot ={
    loader.systemd-boot ={
      enable = true;
      editor= false;
      consoleMode = "max";
      memtest86.enable = true;
    };
    loader.efi.canTouchEfiVariables = true;
  };

  qt = {
    enable = true;
    # style = "null";
    platformTheme = "qt5ct";
  };

 networking ={  
  # proxy.default = "http://user:password@proxy:port/";
  # proxy.noProxy = "127.0.0.1,localhost,internal.domain";
  firewall = {
    enable = true;
    allowedTCPPorts = [ 4455 ]; 
    allowedUDPPorts = [ 4455 ];
  };
  hosts = {
    "192.168.0.11" = [ "home.10leej.com" ];
  };
  hostName = "desktop"; # Define your hostname.
    networkmanager = {
      enable = true;
      #firewallBackend = "iptables"
    };
  };

  # Set your time zone.
  time.timeZone = "America/New_York";

  # Enable sound.
  # sound.enable = true;
  # hardware.pulseaudio.enable = true;
  security = {
    rtkit.enable = true;
    polkit.enable = true;
  };

  virtualisation.libvirtd.enable = true;

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    # you should include the .zip files from humblebundle
    # (callPackage ./packages/wonderdraft.nix {})
    # (callPackage ./packages/dungeondraft.nix {})
    (callPackage ./packages/printer-driver.nix {})

    # cli tools
    greetd.tuigreet wget htop iotop tmux iftop git xdg-utils xdg-user-dirs pulseaudio libva-utils ani-cli intel-gpu-tools glib pciutils nfs-utils python3 unzip

    # graphical environment
    foot rofi-wayland pavucontrol bitwarden mpv rhythmbox yt-dlp mpv waybar qpwgraph
    arc-theme arc-icon-theme fastfetch
    liberation_ttf liberation_ttf_v1 aileron helvetica-neue-lt-std
    grim slurp wl-clipboard imv mako
    sway-contrib.grimshot
    mate.engrampa libsForQt5.qtstyleplugin-kvantum 

    # GUI Applications
    discord element-desktop lutris protonup-qt virt-manager
    chromium firefox thunderbird
    borgbackup borgmatic

    # for the video games
    zeroad # the best RTS for linux

    # music players
    mpd mpc sonata
    
    # content creation tools
    audacity 
    kdePackages.kdenlive
    moonlight-qt # for controlling the stream-PC
    ( wrapOBS.override { obs-studio = pkgs.obs-studio; } {
      plugins = with pkgs.obs-studio-plugins; [
        obs-move-transition
        obs-vkcapture
        obs-backgroundremoval 
        obs-teleport
      ];
    } )
  ];

  # Copy the NixOS configuration file and link it from the resulting system
  # (/run/current-system/configuration.nix). This is useful in case you
  # accidentally delete configuration.nix.
  # system.copySystemConfiguration = true;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It's perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).

  systemd = {
    services = {
      flatpak-repo = {
        wantedBy = [ "multi-user.target" ];
        path = [ pkgs.flatpak ];
        script = ''
          flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
        '';
      };
    };
    user.services.polkit-gnome-authentication-agent-1 = {
      description = "polkit-gnome-authentication-agent-1";
      wantedBy = [ "graphical-session.target" ];
      wants = [ "graphical-session.target" ];
      after = [ "graphical-session.target" ];
      serviceConfig = {
          Type = "simple";
          ExecStart = "${pkgs.polkit_gnome}/libexec/polkit-gnome-authentication-agent-1";
          Restart = "on-failure";
          RestartSec = 1;
          TimeoutStopSec = 10;
        };
    };
  };

  system = { 
    stateVersion = "24.11"; # Did you read the comment?
    #autoUpgrade = {
    #  enable = false;
    #  flake = inputs.self.outPath;
    #  flags = [
    #    "--update-input"
    #    "nixpkgs"
    #    "--no-write-lock-file"
    #    "-L" # print build logs
    #  ];
    #  channel = "https://channels.nixos.org/nixos-24.11";
    #  operation = "boot";
    #  allowReboot = true;
    #  dates = "09:00";
    #  rebootWindow = {
    #    lower = "09:00";
    #    upper = "13:00";
    #  };
    #};
  };
}

